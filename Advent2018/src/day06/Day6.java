package day06;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;


public class Day6 {


	//constructeur ----
	public Day6() {	
	}



	// main ----
	public static void main(String[] args) throws FileNotFoundException {     
		Day6 d = new Day6();
		int[][] grille = new int[1000][1000];
		double startTime = System.currentTimeMillis();
		d.run2(grille);
		//d.run1(grille);
		double endTime = System.currentTimeMillis();
		System.out.println("Time: "+ (endTime - startTime)/1000 + " s");
	}





	// M�thodes ----

	//run1
	public void  run1(int[][] grille) throws FileNotFoundException  {

		Scanner infile = new Scanner(new FileReader("src/day06/day6.txt"));
		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}


		ArrayList<Point> lesPoints = new ArrayList<>();
		for (int index=0;index<input.size(); index++) {

			int x=Integer.parseInt(input.get(index).substring(0, input.get(index).indexOf(",")));
			int y=Integer.parseInt(input.get(index).substring(input.get(index).indexOf(",")+1, input.get(index).length()).trim());

			Point pt=new Point(x,y);
			pt.setNom(index+1);
			lesPoints.add(pt);
		}	


		for (int i=0; i<grille.length;i++) {
			for (int j=0; j<grille.length;j++) {

				//calcul de la distance avec chacun des points
				int dmin=100000000; // = infini
				int pt_plus_proche = 0;
				for(Point pt : lesPoints) {
					int d= Math.abs(pt.getX()-i) + Math.abs(pt.getY()-j);
					if (dmin==d) {pt_plus_proche=0;}//deux points sont � m�me distance
					else
						if (d<dmin) {dmin=d;pt_plus_proche=pt.getNom();}			
				}

				if (pt_plus_proche != 0) {
					lesPoints.get(pt_plus_proche-1).setNb_satellites(lesPoints.get(pt_plus_proche-1).getNb_satellites()+1);
				}

				//gestion des exterieurs/infinis
				if (pt_plus_proche != 0 && (i==0 || i==grille.length-1 || j==0 || j==grille.length-1)) {
					lesPoints.get(pt_plus_proche-1).setNb_satellites(1000000);
				}					
			}


		}// fin boucle principale
		// Affichage des r�sultats
		int taille_maximale=0;
		for(Point pt : lesPoints) {

			if (pt.getNb_satellites() <1000000) {
				System.out.println("nom: "+pt.getNom());
				System.out.println("nb sat: "+pt.getNb_satellites());
				if (pt.getNb_satellites()>taille_maximale) {taille_maximale=pt.getNb_satellites();}			
			}	
		}
		System.out.println("-------------------------");
		System.out.println("taille_maximale: "+taille_maximale);
	}




	//run2
	public void  run2(int[][] grille) throws FileNotFoundException  {

		Scanner infile = new Scanner(new FileReader("src/day06/day6.txt"));		ArrayList<String> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextLine());
		}

		ArrayList<Point> lesPoints = new ArrayList<>();
		for (int index=0;index<input.size(); index++) {

			int x=Integer.parseInt(input.get(index).substring(0, input.get(index).indexOf(",")));
			int y=Integer.parseInt(input.get(index).substring(input.get(index).indexOf(",")+1, input.get(index).length()).trim());

			Point pt=new Point(x,y);
			pt.setNom(index+1);
			lesPoints.add(pt);
		}	

		int nb_points = 0;
		for (int i=0; i<grille.length;i++) {
			for (int j=0; j<grille.length;j++) {

				//calcul de la distance avec chacun des points
				int distance_totale_au_point_pt=0;

				for(Point pt : lesPoints) {
					int d= Math.abs(pt.getX()-i) + Math.abs(pt.getY()-j);
					distance_totale_au_point_pt=d+distance_totale_au_point_pt;					
				}
				if(distance_totale_au_point_pt<10000) {nb_points=nb_points+1;}
			}
		}
		// Affichage des r�sultats
		System.out.println("nb points dans la r�gion: "+nb_points);
	}
}



