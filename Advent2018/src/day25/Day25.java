package day25;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;


public class Day25 {
//main --------
	public static void main(String[] args) throws FileNotFoundException {     
		Day25 d = new Day25();
		double startTime = System.currentTimeMillis();		
		String file="day25";
		d.run1(file);
		double endTime = System.currentTimeMillis();
		System.out.println("Time: "+ (endTime - startTime)/1000 + " s");
	}

	
	
private void run1(String file) throws FileNotFoundException {
	ArrayList<Point> points = importation(file);
	int[][] distances = creationMatriceDistance(points);
	ajouterLiens(points, distances);
	
}



private void ajouterLiens(ArrayList<Point> points, int[][] distances) {

	ArrayList<ArrayList<Integer>> adj =	new ArrayList<ArrayList<Integer>>(points.size());
	for (int i = 0; i < points.size(); i++) {
		adj.add(new ArrayList<Integer>());
	}

	Graph g = new Graph(points.size());
	for (int j=0; j<points.size();j++) {
		for (int i=j; i<points.size();i++) {
			//System.out.println(distances[i][j]);
			if (distances[i][j] != -1 & distances[i][j] <= 3){
				g.addEdge(i, j);
	


			}
		}
	}
	g.connectedComponents();

}

//private static void addEdge(ArrayList<ArrayList<Integer>> adj, int i, int j){
//	adj.get(i).add(j);
//	adj.get(j).add(i);
//}

private int[][] creationMatriceDistance(ArrayList<Point> points) {

	int[][] distances = new int[points.size()][points.size()];
	for (int i=0;i<points.size();i++) {
		for (int j=0;j<points.size();j++) {
			if (i > j) {
				int d=distance(points.get(i), points.get(j));
				distances[i][j]=d;
				//				System.out.println(i);
				//				System.out.println(j);
				//				System.out.println(d);
				//				System.out.println("------");
			}else {
				distances[i][j]=-1; // autre c�t� de la matrice diagonale dont on ne s'occupera pas.
			}
		}
	}
	return distances;
}



private int distance(Point point, Point point2) {
	return (Math.abs((point.getX()-point2.getX())) + Math.abs((point.getY()-point2.getY())) +
			Math.abs((point.getZ()-point2.getZ())) + Math.abs((point.getT()-point2.getT())) );	
}



private ArrayList<Point> importation(String file) throws FileNotFoundException {
	Scanner infile = new Scanner(new FileReader("src/day25/"+file+".txt"));
	ArrayList<String> input = new ArrayList<>();
	while (infile.hasNext()) {
		input.add(infile.nextLine());
	}

	//ajouter les points
	ArrayList<Point> points = new ArrayList<>();
	for (String str: input) {
		String[] strSplitted=str.split(",");
		int x= Integer.parseInt(strSplitted[0].strip());
		int y= Integer.parseInt(strSplitted[1].strip());
		int z= Integer.parseInt(strSplitted[2].strip());
		int t= Integer.parseInt(strSplitted[3].strip());
		Point point = new Point(x,y,z,t);
		points.add(point);		
	}
	
return points;
	
}	
	
	
}
