package day01;
import java.io.*;
import java.util.*;


public class Day1 {

	//constructeur
	public Day1() {	
	}


	// main
	public static void main(String[] args) throws FileNotFoundException {     
		Day1 d = new Day1();
		double startTime = System.currentTimeMillis();
		d.run2();
		double endTime = System.currentTimeMillis();
		System.out.println("Time: "+ (endTime - startTime)/1000 + " s");
	}


	//run1
	public void run1() throws FileNotFoundException {
		int somme = 0;

		Scanner infile = new Scanner(new FileReader("src/day01/day1a.txt"));

		while (infile.hasNext()) {
			//System.out.println(somme);
			somme = somme + infile.nextInt();
		}

		System.out.println("Somme = " + somme);
	}


	//run2
	public void run2() throws FileNotFoundException {  

		//import
		Scanner infile = new Scanner(new FileReader("src/day01/day1a.txt"));
		ArrayList<Integer> input = new ArrayList<>();
		while (infile.hasNext()) {
			input.add(infile.nextInt());
		}
		//System.out.println("ma liste: " +input);


		// Traitements
		int somme = 0;
		ArrayList<Integer> liste_histo = new ArrayList<>();
		liste_histo.add(0);
		boolean flag=true;
		int i=0;
		while (flag && i<input.size()) {

			somme=somme+ input.get(i) ;
			//System.out.print(somme);
			i=i+1;
			if (i==input.size()) {i=0;}

			if (liste_histo.contains(somme)) {
				flag=false;
				System.out.println("Le nombre doublé est: "+somme);
			}
			else {
				liste_histo.add(somme);
			}				
		}			
	} 
}
